import sqlite3
import datetime

from flask import Flask, g, jsonify, request
from collections import OrderedDict
from flask_cors import CORS


DATABASE = "sasiad.db"

app = Flask(__name__)
app.config["JSON_SORT_KEYS"] = False
CORS(app)


def get_db():
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        # in case there is no proper table in database
    db.execute(
        "CREATE TABLE IF NOT EXISTS Users (Id INTEGER PRIMARY KEY, name TEXT, surename TEXT, password TEXT, email TEXT UNIQUE, created_date TEXT, school TEXT, student_id TEXT)"
    )
    db.execute(
        "CREATE TABLE IF NOT EXISTS Offers (Id INTEGER PRIMARY KEY, name TEXT, category TEXT, description TEXT, author TEXT, created_date TEXT)"
    )
    db.execute(
        "CREATE TABLE IF NOT EXISTS Help (Id INTEGER PRIMARY KEY, name TEXT, category TEXT, description TEXT, author TEXT, created_date TEXT)"
    )
    
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, "_database", None)
    if db is not None:
        db.close()


@app.route("/")
def hello_world():
    return "Welcome to Student!"


@app.route("/register", methods=['POST'])   #Da sie założyć konto bez hasła. Zrobić minimalność ilość znaków 
def register_user():
    if request.method == 'POST':
        user_data = request.get_json()

        created_date = str(
            datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        )
        name = user_data['name'] if 'name' in user_data.keys() else None
        surename = user_data['surename'] if 'surename' in user_data.keys() else None
        password = user_data['password'] if 'password' in user_data.keys() else None
        email = user_data['email'] if 'email' in user_data.keys() else None
        school = user_data['school'] if 'school' in user_data.keys() else None
        index = user_data['index'] if 'index' in user_data.keys() else None

        if name is not None and surename is not None and password is not None and email is not None and school is not None and index is not None:
            if len(name) and len(surename) and len(password) > 3 and len(email) and len(index) and len(school):
                db = get_db()
                db.execute(
                    "INSERT INTO Users (name, surename, password, email, created_date, school, student_id) VALUES (?, ?, ?, ?, ?, ?, ?)", 
                    (name, surename, password, email, created_date, school, index)
                )
                db.commit()
                return 'Success!', 200
            else:
                return 'Provided data is not correct', 400
        else:
            return 'Not enough data provided!', 400



@app.route("/login", methods=['POST'])
def login():
    if request.method == 'POST':
        user_data = request.get_json()
        
        email = user_data['email'] if 'email' in user_data.keys() else None
        password = user_data['password'] if 'password' in user_data.keys() else None

        if email is not None and password is not None:
            db = get_db()
            user = db.execute(
                "SELECT * FROM Users WHERE email=(?)", ([email])
            ).fetchone()
            if user is not None:
                db_user = OrderedDict()
                db_user['Id'] = user[0]
                db_user['Name'] = user[1]
                db_user['Surname'] = user[2]
                db_user['email'] = user[4]
                db_user['indeks'] = user[7]
                db_user['school'] = user[6]
                db_user['password'] = user[3]
                if password == db_user['password']:
                    fields = ['Id', 'name', 'surname', 'password', 'email', 'created_date', 'school', 'index']
                    return jsonify(dict(zip(fields, user))), 200
                else:
                    return jsonify('Wrong password'), 401
            return 'email not found!', 401
        


@app.route("/list_users")
def list_users():
    db = get_db()
    users = db.execute(
        "SELECT email FROM Users"
    ).fetchall()
    return jsonify(users), 200


@app.route("/hello_user/<user_id>")
def hello_user(user_id):
    db = get_db()
    user = db.execute(
        "SELECT name, surename FROM Users WHERE Id = (?)", ([user_id])
    ).fetchone()
    return jsonify(user)


@app.route("/create_ad", methods=['POST'])
def create_ad():
    data = request.get_json()
    db = get_db()
    db.execute(
        "INSERT INTO Offers (name, category, description, author, created_date) VALUES (?, ?, ?, ?, ?)",
        (
            data['name'], data['category'], data['description'], data['author'], str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"))
        )
    )
    db.commit()
    return 'success', 200


@app.route("/offers/<user_id>")
def offers(user_id):
    db = get_db()
    pom = db.execute('SELECT school FROM Users WHERE Id = (?)', ([user_id])).fetchone()
    if pom is None:
        return 'Error', 400
    school = pom[0]
    
    res= db.execute('SELECT Offers.name, Offers.description, Users.name, Users.surename, Offers.created_date FROM Offers INNER JOIN Users ON Offers.author=Users.Id WHERE Users.school = (?)',([school])).fetchall()
    fields = ['title', 'description', 'name','surname','created']
    ehh = list()

    res = list(res)
    
    for i in res:
        ehh.append((dict(zip(fields, i))))
    return jsonify(ehh), 200
    


@app.route("/create_help", methods=['POST'])
def create_help():
    data = request.get_json()
    db = get_db()
    db.execute(
        "INSERT INTO Help (name, category, description, author, created_date) VALUES (?, ?, ?, ?, ?)",
        (
            data['name'], data['category'], data['description'], data['author'], str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"))
        )
    )
    db.commit()
    return 'success', 200


@app.route("/help/<user_id>")
def help(user_id):
    db = get_db()
    pom = db.execute('SELECT school FROM Users WHERE Id = (?)', ([user_id])).fetchone()
    if pom is None:
        return 'Error', 400
    school = pom[0]
    res = db.execute('SELECT Help.name, Help.description, Users.name, Users.surename,  Help.created_date FROM Help INNER JOIN Users ON Help.author=Users.Id WHERE Users.school = (?)',([school])).fetchall()
    fields = ['title', 'description', 'name','surname','created']
    ehh = list()

    res = list(res)
    
    for i in res:
        ehh.append((dict(zip(fields, i))))
    return jsonify(ehh), 200



@app.route('/chat', methods=['GET','POST'])
def chat():
    if request.method == 'GET':
        data = request.get_json()
        sender = data['sender']
        reciver = data['reciver']
        talk = str(sender) + '_' + str(reciver)
        talk2 = str(reciver) + '_' +  str(sender)

        try:
            fh = open(talk, 'r')
            res =fh.read()
            res2 = '['+res+']'
            fh.close()
            return jsonify(res2)
        except FileNotFoundError:
            pass
        try:
            fh = open(talk2, 'r')
            res =fh.read()
            res2 = '['+res+']'
            fh.close()
            return jsonify(res2)
        except FileNotFoundError:
            pass

        return '', 200

    if request.method == 'POST':
        data = request.get_json()
        sender = data['sender']
        reciver = data['reciver']
        mess = data['message']
        mess = '{' +'\"'+sender+'\"'+":"+'\"'+mess+'\"'+'},'
        print(mess)
        talk = str(sender) + '_' + str(reciver)
        talk2 = str(reciver) + '_' +  str(sender)
        try:
            fh = open(talk, 'a')
            fh.write(mess)
            fh.close()
            return '', 200
        except FileNotFoundError:
            pass
        try:
            fh = open(talk2, 'a+')
            fh.write(mess)
            fh.close()
            return '', 200
        except FileNotFoundError:
            pass

if __name__ == "__main__":
    app.run(debug=True)